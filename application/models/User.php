<?php

class User extends CI_Model{

	private $api_url = 'http://10.8.3.219/gudtrak_api/';

	function __construct(){
		parent::__construct();
	}
	
	public function login_via_api($data)
	{

		$method 	= 'POST';
		$url 		= $this->api_url.'login/auth';
		$response 	= set_curl($url,$data,$method);

		return $response;
	}

	public function get_data_user()
	{
		$data 		= array("id_user" => $this->session->userdata('user_id'));
		$method 	= 'POST';
		$url 		= $this->api_url.'login/collect';
		$response 	= get_api_data($url,$data,$method);

		return json_decode($response);

	}

	public function test($id_perusahaan_angkutan=null)
	{
	    $data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/supir';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}

	

}