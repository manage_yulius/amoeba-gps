<?php

class Kendaraan extends CI_Model{

	private $api_url = 'http://10.8.3.219/gudtrak_api/';

	function __construct(){
		parent::__construct();
	}
	
	public function get_kendaraan($id_perusahaan_angkutan=null)
	{
	    $data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/kendaraan';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}

	public function get_supir($id_perusahaan_angkutan=null)
	{
	    $data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/supir';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}

	public function get_kernet($id_perusahaan_angkutan=null)
	{
	    $data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/kernet';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}

	public function get_device($id_perusahaan_angkutan=null)
	{
	    $data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/device';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}

	public function get_asuransi($id_perusahaan_angkutan=null)
	{
	    $data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/asuransi';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}

	public function get_perusahaan($id_perusahaan_angkutan=null)
	{
	    $data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/perusahaan';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}

	public function get_driver_expired($id_perusahaan_angkutan=null)
	{
		$data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/driver_expired';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}

	public function get_list_truck($id_perusahaan_angkutan=null)
	{
		$data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/list_truck';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}

	public function get_list_truck_a($id_perusahaan_angkutan=null)
	{
		$data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/list_truck_a';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}

	public function get_list_driver_a($id_perusahaan_angkutan=null)
	{
		$data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/list_driver_a';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}

	public function get_list_asuransi_a($id_perusahaan_angkutan=null)
	{
		$data 		= array("id_perusahaan_angkutan" => $id_perusahaan_angkutan);
		$method 	= 'POST';
		$url 		= $this->api_url.'car/list_asuransi_a';
		$response 	= get_api_data($url,$data,$method);
		return json_decode($response);
	}	



}