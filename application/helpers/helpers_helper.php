<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Input */

if ( ! function_exists('post'))
{
	function post($str = '')
	{
		$CI =& get_instance();
		
		//return $CI->db->escape_str($CI->input->post($str));
		return $CI->input->post($str);
	}
}

if ( ! function_exists('get'))
{
	function get($str = '')
	{
		$CI =& get_instance();
		return $CI->input->get($str);
	}
}


/* load  */

if ( ! function_exists('model'))
{
	function model($model = '', $alias = '')
	{
		$CI =& get_instance();
	
		if($alias){
			$CI->load->model($model, $alias);
			return $CI->$alias;
		}else{
			$CI->load->model($model);
			$modelName = explode('/', $model);
			$modelName = $modelName[count($modelName) - 1];
			return $CI->$modelName;
		}
	}
}

if ( ! function_exists('library'))
{
	function library($lib = '')
	{
		$libName = explode('/', $lib);
		
		$CI =& get_instance();
		$CI->load->library($lib);
		
		$libName = $libName[count($libName) - 1];
		return $CI->{$libName};
	}
}

if ( ! function_exists('helper'))
{
	function helper($helper = '')
	{
		$CI =& get_instance();
		$CI->load->helper($helper);
	}
}

/* View */

if ( ! function_exists('view'))
{
	function view($view = '', $data = '')
	{
		$CI =& get_instance();
		
		if(! isset($data)) {
			return $CI->load->view($view);
		}
		return $CI->load->view($view, $data);
	}
}

if(!function_exists('jumlahSama')){
	function jumlahSama(){
		$numArg 	= func_num_args();
		$flagSama 	= true;
		
		if($numArg){
			$lastArg = func_get_arg(0);
			for($i = 1; $i < $numArg; $i++){
				$currArg = func_get_arg(1);
				if(!is_array($currArg) || count($lastArg) != count($currArg)){
					// Lansung cabut, udah gak sama
					return false;
				}
				
				$lastArg = $currArg;
			}
		}else{
			// Return True aja soalnya cuma 1 atau gak ada sama sekali
			return true;	
		}
		
		return true;
	}
}

if(!function_exists('formatUang')){
	function formatUang($n){
		$n = str_replace('.', '', $n);
		$n = str_replace(',', '.', $n);
		
		return number_format($n, 0, ',', '.');	
	}
}


if(!function_exists('memUsage')){
	function memUsage($var){
		$before = memory_get_usage();
		$clone = $var;
		$after = memory_get_usage();
		
		return $after - $before;
	}
}

if(!function_exists('gridHeader')){
	function gridHeader($field, $label, $cfg){
		$urlAdd = ($cfg->keyword ? "/kw:$cfg->keyword" : '').($cfg->searchField ? "/sf:$cfg->searchField" : '');
		
		if($cfg->sortField == $field){
			if($cfg->sortMethod == 'ASC'){
				$url = "$cfg->base/ob:$field@DESC".$urlAdd;
			}else{
				$url = "$cfg->base/ob:$field@ASC".$urlAdd;
			}
			
			$class = strtolower($cfg->sortMethod);
		}else{
			$url = "$cfg->base/ob:$field@ASC".$urlAdd;
			$class = '';
		}
	?>
	<a href="<?php echo site_url($url) ?>" class="<?php echo $class ?>"><?php echo $label ?></a>
	<?php
	}
}

if(!function_exists('pretty_print_json')){
	function pretty_print_json( $json )
	{
		$result = '';
		$level = 0;
		$in_quotes = false;
		$in_escape = false;
		$ends_line_level = NULL;
		$json_length = strlen( $json );
	
		for( $i = 0; $i < $json_length; $i++ ) {
			$char = $json[$i];
			$new_line_level = NULL;
			$post = "";
			if( $ends_line_level !== NULL ) {
				$new_line_level = $ends_line_level;
				$ends_line_level = NULL;
			}
			if ( $in_escape ) {
				$in_escape = false;
			} else if( $char === '"' ) {
				$in_quotes = !$in_quotes;
			} else if( ! $in_quotes ) {
				switch( $char ) {
					case '}': case ']':
						$level--;
						$ends_line_level = NULL;
						$new_line_level = $level;
						break;
	
					case '{': case '[':
						$level++;
					case ',':
						$ends_line_level = $level;
						break;
	
					case ':':
						$post = " ";
						break;
	
					case " ": case "\t": case "\n": case "\r":
						$char = "";
						$ends_line_level = $new_line_level;
						$new_line_level = NULL;
						break;
				}
			} else if ( $char === '\\' ) {
				$in_escape = true;
			}
			if( $new_line_level !== NULL ) {
				$result .= "\n".str_repeat( "\t", $new_line_level );
			}
			$result .= $char.$post;
		}
	
		return $result;
	}
}

if(!function_exists('debux')){
	function debux($var,$die=false){
		echo '<pre>';
		print_r($var);
		echo '</pre>';
		($die) ? die : '';
	}
}

if(!function_exists('curl_traccar')){
	function curl_traccar($url,$data,$method)
	{
		$response 	= "";
		$headers	= array('Content-Type: application/x-www-form-urlencoded');
		$param		= "email=$data[email]&password=$data[password]";
		$ch 		= curl_init();


		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		if ($method == 'POST' || $method == 'PUT' || $method == 'DELETE') {
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS,$param);
	    }

	    curl_setopt($ch, CURLOPT_HEADER, false);
	    //get the default response headers 
	 
	    header('Content-Type: application/x-www-form-urlencoded');
	    header("Access-Control-Allow-Origin:http://10.8.3.219:8087");

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec($ch);
		curl_close ($ch);

		return $server_output;
	}
}



if(!function_exists('set_curl')){
	function set_curl($url,$data,$method)
	{
		$response 	= "";
		$param		= "email=$data[email]&password=$data[password]";
		
		$ch 		= curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
	    if ($method == 'POST' || $method == 'PUT' || $method == 'DELETE') {
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
	    }
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($ch);
	    curl_close($ch);

	    return $response;

	}
}

if(!function_exists('get_api_data')){
	function get_api_data($url,$data,$method)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$response = curl_exec($ch);
		curl_close($ch);
	    return $response;

	}
}

if(!function_exists('is_login')){
	function is_login()
	{
		$CI =& get_instance();
		if(empty($CI->session->userdata('PHPSESSID'))){
			redirect(base_url().'utama/login');
		}
	}
}

if(!function_exists('generate_datatable_api')){
	function generate_datatable_api($data)
	{
		echo '<table class="table table-striped table-bordered datatable" style="width:100%">';
		echo '<thead>';
		echo '<tr>';

		foreach ($data->response[0] as $key => $value) {
			echo '<th>'.ucwords(str_replace('_',' ',$key)).'</th>';
		}

		echo '</tr>';
		echo '</thead>';
		echo '<tbody>';
		
		foreach ($data->response as $key => $value) {
			echo '<tr>';
			foreach ($data->response[$key] as $keyx => $valuex) {
				echo '<td>'.$valuex.'</td>';
			}
			echo '</tr>';
		}

		echo '</tbody>';
		echo '</table>';

        echo '<script>
		    	$(document).ready(function() {
			         $(".datatable").DataTable();
			    } );</script>';
	}
}