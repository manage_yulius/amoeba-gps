<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Database extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('kendaraan','user'));
		is_login();
	}


	public function truck()
	{

		$id_perusahaan_angkutan = $this->session->userdata('id_perusahaan_angkutan');
		#hardcode data kosong
		$id_perusahaan_angkutan = 1;

		$data['caption']  	= 'List Truck';
		$data['list_data']  = $this->kendaraan->get_list_truck_a($id_perusahaan_angkutan);

		$this->load->view('data_truck',$data);
	}

	public function driver()
	{
		$id_perusahaan_angkutan = $this->session->userdata('id_perusahaan_angkutan');
		#hardcode data kosong
		$id_perusahaan_angkutan = 1;

		$data['caption']  	= 'List Driver';
		$data['list_data']  = $this->kendaraan->get_list_driver_a($id_perusahaan_angkutan);

		$this->load->view('data_truck',$data);
	}

	public function asuransi()
	{
		$id_perusahaan_angkutan = $this->session->userdata('id_perusahaan_angkutan');
		#hardcode data kosong
		$id_perusahaan_angkutan = '';

		$data['caption']  	= 'List Asuransi';
		$data['list_data']  = $this->kendaraan->get_list_asuransi_a($id_perusahaan_angkutan);

		$this->load->view('data_truck',$data);
	}




}