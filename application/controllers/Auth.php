<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
demo untuk desain frontend
*/

class Auth extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function register()
	{
		$this->load->view('register');
	}

	public function driver()
	{
		$this->load->view('regis_driver');
	}

	public function kernet()
	{
		$this->load->view('regis_karnet');
	}

	public function upload()
	{
		$this->load->view('upload_data');
	}

	public function forum()
	{
		$this->load->view('forum');
	}
}
