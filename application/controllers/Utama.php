<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
demo untuk desain frontend
*/

class Utama extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function dashboard()
	{
		$this->load->view('utama');
	}

	public function homepage()
	{
		$this->load->view('dashboard');
	}

	public function daftar()
	{
		$this->load->view('daftar');
	}

	public function index()
	{
		$this->load->view('login');
	}
}
