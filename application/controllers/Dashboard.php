<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('kendaraan','user'));
	}

	public function guddata()
	{
		is_login();

		$id_angkutan = $this->session->userdata('id_perusahaan_angkutan');

		#by pass id_angkutan langsung biar ga 0
		$id_angkutan = '1';

		if(!empty($id_angkutan)):
			$data['total_kendaraan']	= $this->kendaraan->get_kendaraan($id_angkutan)->response;
			$data['total_supir']		= $this->kendaraan->get_supir($id_angkutan)->response;
			$data['total_kernet']		= $this->kendaraan->get_kernet($id_angkutan)->response;
			$data['total_device']		= $this->kendaraan->get_device($id_angkutan)->response;
			$data['total_asuransi']		= $this->kendaraan->get_asuransi($id_angkutan)->response;
			$data['total_perusahaan']	= $this->kendaraan->get_perusahaan($id_angkutan)->response;
		else:
			$data['total_kendaraan']	= $this->kendaraan->get_kendaraan()->response;
			$data['total_supir']		= $this->kendaraan->get_supir()->response;
			$data['total_kernet']		= $this->kendaraan->get_kernet()->response;
			$data['total_device']		= $this->kendaraan->get_device()->response;
			$data['total_asuransi']		= $this->kendaraan->get_asuransi()->response;
			$data['total_perusahaan']	= $this->kendaraan->get_perusahaan()->response;
		endif;

		$data['driver_expired']		= $this->kendaraan->get_driver_expired($id_angkutan);
		$data['expired']['jumlah']	= count($data['driver_expired']->response);
		$now = date('Y-m-d');
		$_3bulan = 0;
		$_6bulan = 0;
		$_9bulan = 0;
		$start_date = new DateTime();
		foreach ($data['driver_expired']->response as $key => $value) {
			
			$end_date 	= new DateTime($value->habis_berlaku_sim);
			$interval = $start_date->diff($end_date);
			
			if($interval->m>=1 AND $interval->m<=3){
				$_3bulan+=1;
			}elseif($interval->m>=1 AND $interval->m<=6){
				$_6bulan+=1;
			}elseif($interval->m>=1 AND $interval->m<=9){
				$_9bulan+=1;
			}

		}

		$data['expired']['3bulan']['angka']  = $_3bulan;
		$data['expired']['3bulan']['persen'] = round(($_3bulan/$data['expired']['jumlah'])*100);
		$data['expired']['6bulan']['angka']  = $_6bulan;
		$data['expired']['6bulan']['persen'] = round(($_6bulan/$data['expired']['jumlah'])*100);
		$data['expired']['9bulan']['angka']  = $_9bulan;
		$data['expired']['9bulan']['persen'] = round(($_9bulan/$data['expired']['jumlah'])*100);

		$this->load->view('utama',$data);
	}

	public function index()
	{
		is_login();

		$this->load->view('dashboard');
	}
	public function get_data_via_ajax()
	{

		$id_angkutan = $this->session->userdata('id_perusahaan_angkutan');
		#by pass id_angkutan langsung biar ga 0
		$id_angkutan = '1';

		$var = $_POST['func'];
		if($var=='list_truck'){
			$data['list'] = $this->kendaraan->get_list_truck_a($id_angkutan);
		}else{
			debux('<h2><center>Under Construction</center></h2>',true);
		}

		$this->generate_table_modal($data['list']);	

	}

	#datatableny clientside dulu ya
	public function generate_table_modal($data)
	{
		
		echo '<table class="table table-striped table-bordered datatable" style="width:100%">';
		echo '<thead>';
		echo '<tr>';

		foreach ($data->response[0] as $key => $value) {
			echo '<th>'.ucwords(str_replace('_',' ',$key)).'</th>';
		}

		echo '</tr>';
		echo '</thead>';
		echo '<tbody>';
		
		foreach ($data->response as $key => $value) {
			echo '<tr>';
			foreach ($data->response[$key] as $keyx => $valuex) {
				echo '<td>'.$valuex.'</td>';
			}
			echo '</tr>';
		}

		echo '</tbody>';
		echo '</table>';

        echo '<script>
		    	$(document).ready(function() {
			         $(".datatable").DataTable();
			    } );</script>';

	}

	public function get_data_user()
	{
		$data['detail_user'] = $this->user->get_data_user();

		foreach ($data['detail_user']->payload[0] as $key => $value) {
			$sess[$key] = $value;
		}

		$this->session->set_userdata($sess);
	}

	public function login()
	{	
	
		$ress 		= array();
	    $data 		= array("email"		=> $this->input->post('email'),
					  		"password"  => $this->input->post('password'));

		$response   = $this->user->login_via_api($data);
		$result		= json_decode($response);

		if($result->is_error!=1){
	    	
	    	foreach ($result->payload as $key => $value) {
	    		$ress[$key] = $value;
	    	}

			$this->session->set_userdata($ress);
	    }

		echo $response;

	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/Auth');
	}

}