<?php include('layout/dashboard/header.php'); ?>

<?php include('layout/dashboard/nav.php'); ?>

<!-- ############ PAGE START-->
<div class="row-col b-b body-margin-top">
  <div class="col-md">
    <div class="padding">
        <div class="row">         
          <div class="col-sm-12">
            <div class="box">
            <div class="box-header">
              <!-- <h1 class="h1-custom"><i class="fa fa-users" aria-hidden="true"></i> Forum</h1>
              <small>Data-data truk yang sudah terdaftarkan</small> -->
              <!-- <?php #debux($expired); ?> -->
            </div>
            <div class="box-body">
            	<div class="row">
                <div class="col-md-4">
                  <form>
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Search Message or Name...">
                      <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                          <i class="glyphicon glyphicon-search"></i>
                        </button>
                      </div>
                    </div>
                  </form>
                  <div class="p-a-md">
                    <a href class="pull-right"><span class="label blue">20</span></a>
                    <h6>Inbox</h6>
                    <small>10 message need answers</small>
                    <ul class="list no-padding">
                      <li class="list-item">
                        <a herf class="list-left">
                          <span class="w-40 avatar red-100">
                            <span>C</span>
                            <i class="on b-white bottom"></i>
                          </span>
                        </a>
                        <div class="list-body">
                          <div><a href>Chris Fox</a></div>
                          <a href class="text-muted">How to create an icon like the demo app</a>
                          <br>
                          <div class="m-y-sm pull-left">
                            <span class="text-muted">5 Januari 2019</span>
                          </div>
                          <div class="m-y-sm pull-right">
                            <a href class="btn btn-xs white"><i class="fa fa-pencil"></i></a>
                            <a href class="btn btn-xs white"><i class="fa fa-remove"></i></a>
                          </div>
                        </div>
                      </li>
                      <li class="list-item">
                        <a herf class="list-left">
                          <span class="w-40 avatar purple-100">
                            <span>M</span>
                            <i class="on b-white bottom"></i>
                          </span>
                        </a>
                        <div class="list-body">
                          <div><a href>Mogen Polish</a></div>
                          <a href class="text-muted">How to build my custom color</a>
                          <br>
                          <div class="m-y-sm pull-left">
                            <span class="text-muted">5 Januari 2019</span>
                          </div>
                          <div class="m-y-sm pull-right">
                            <a href class="btn btn-xs white"><i class="fa fa-pencil"></i></a>
                            <a href class="btn btn-xs white"><i class="fa fa-remove"></i></a>
                          </div>
                        </div>
                      </li>
                      <li class="list-item">
                        <a herf class="list-left">
                          <span class="w-40 avatar blue-200">
                            <span>J</span>
                            <i class="off b-white bottom"></i>
                          </span>
                        </a>
                        <div class="list-body">
                          <div><a href>Joge Lucky</a></div>
                          <a href class="text-muted">What is the app requriements</a>
                          <br>
                          <div class="m-y-sm pull-left">
                            <span class="text-muted">5 Januari 2019</span>
                          </div>
                          <div class="m-y-sm pull-right">
                            <a href class="btn btn-xs white"><i class="fa fa-pencil"></i></a>
                            <a href class="btn btn-xs white"><i class="fa fa-remove"></i></a>
                          </div>
                        </div>
                      </li>
                      <li class="list-item">
                        <a herf class="list-left">
                          <span class="w-40 avatar warning">
                            <span>F</span>
                            <i class="on b-white bottom"></i>
                          </span>
                        </a>
                        <div class="list-body">
                          <div><a href>Folisise Chosielie</a></div>
                          <a href class="text-muted">Where to find the API</a>
                          <br>
                          <div class="m-y-sm pull-left">
                            <span class="text-muted">5 Januari 2019</span>
                          </div>
                          <div class="m-y-sm pull-right">
                            <a href class="btn btn-xs white"><i class="fa fa-pencil"></i></a>
                            <a href class="btn btn-xs white"><i class="fa fa-remove"></i></a>
                          </div>
                        </div>
                      </li>
                      <li class="list-item">
                        <a herf class="list-left">
                          <span class="w-40 avatar blue-200">
                            <span>J</span>
                            <i class="off b-white bottom"></i>
                          </span>
                        </a>
                        <div class="list-body">
                          <div><a href>John Lenong</a></div>
                          <a href class="text-muted">What is the app requriements</a>
                          <br>
                          <div class="m-y-sm pull-left">
                            <span class="text-muted">5 Januari 2019</span>
                          </div>
                          <div class="m-y-sm pull-right">
                            <a href class="btn btn-xs white"><i class="fa fa-pencil"></i></a>
                            <a href class="btn btn-xs white"><i class="fa fa-remove"></i></a>
                          </div>
                        </div>
                      </li>
                      <li class="list-item">
                        <a herf class="list-left">
                          <span class="w-40 avatar green-100">
                            <span>P</span>
                            <i class="away b-white bottom"></i>
                          </span>
                        </a>
                        <div class="list-body">
                          <div><a href>Peter</a></div>
                          <a href class="text-muted">How to add my router</a>
                          <br>
                          <div class="m-y-sm pull-left">
                            <span class="text-muted">5 Januari 2019</span>
                          </div>
                          <div class="m-y-sm pull-right">
                            <a href class="btn btn-xs white"><i class="fa fa-pencil"></i></a>
                            <a href class="btn btn-xs white"><i class="fa fa-remove"></i></a>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>  
                <div class="col-sm-8">
                  <div class="p-a-md">
                    <h3>Message <span class="label"> from</span></h3>
                    <br>
                    <ul class="list no-padding">
                        <li class="list-item">
                          <a herf class="list-left">
                            <span class="w-40 avatar red-100">
                              <span>C</span>
                            </span>
                          </a>
                          <div class="list-body">
                            <div>
                              <span>Chris Fox</span>
                              <span class="pull-right text-grey">5 Januari 2019</span>
                            </div>
                            <div class="text-sm">
                              <span class="text-muted">to <b>me</b></span>
                            </div>
                            <br>
                            <hr>
                            <br>
                            <p class="text-grey">
                              Hi GudData,
                              <br><br>
                              How to create an icon like the demo app asdasd asd aslfgsidkjadfkaf
                              as fasf FSDGASFGJSHLAsddsgs  sd ds gds s gdsgdsg h gdsdsdsfds How to create an icon like the demo app asdasd asd aslfgsidkjadfkaf
                              as fasf FSDGASFGJSHLAsddsgs  sd ds gds s gdsgdsg h gdsdsdsfds How to create an icon like the demo app asdasd asd aslfgsidkjadfkaf
                              as fasf FSDGASFGJSHLAsddsgs  sd ds gds s gdsgdsg h gdsdsdsfds How to create an icon like the demo app asdasd asd aslfgsidkjadfkaf
                              as fasf FSDGASFGJSHLAsddsgs  sd ds gds s gdsgdsg h gdsdsdsfds How to create an icon like the demo app asdasd asd aslfgsidkjadfkaf
                              as fasf FSDGASFGJSHLAsddsgs  sd ds gds s gdsgdsg h gdsdsdsfds 
                              <br><br>
                              How to create an icon like the demo app asdasd asd aslfgsidkjadfkaf
                              as fasf FSDGASFGJSHLAsddsgs  sd ds gds s gdsgdsg h gdsdsdsfds How to create an icon like the demo app asdasd asd aslfgsidkjadfkaf
                              as fasf FSDGASFGJSHLAsddsgs  sd ds gds s gdsgdsg h gdsdsdsfds How to create an icon like the demo app asdasd asd aslfgsidkjadfkaf
                              as fasf FSDGASFGJSHLAsddsgs  sd ds gds s gdsgdsg h gdsdsdsfds How to create an icon like the demo app asdasd asd aslfgsidkjadfkaf
                              as fasf FSDGASFGJSHLAsddsgs  sd ds gds s gdsgdsg h gdsdsdsfds How to create an icon like the demo app asdasd asd aslfgsidkjadfkaf
                              as fasf FSDGASFGJSHLAsddsgs  sd ds gds s gdsgdsg h gdsdsdsfds 
                            </p>
                            <br><br>
                            <form>
                              <textarea class="form-control no-border" rows="3" placeholder="Reply message..."></textarea>
                            </form>
                            <div class="box-footer clearfix">
                              <button class="btn btn-info pull-right btn-md">Reply</button>
                              <ul class="nav nav-pills nav-sm">
                                <li class="nav-item"><a class="nav-link" href><i class="fa fa-camera text-muted"></i></a></li>
                                <li class="nav-item"><a class="nav-link" href><i class="fa fa-video-camera text-muted"></i></a></li>
                              </ul>
                            </div>
                          </div>
                        </li>
                    </ul>
                  </div>
                </div>          		
        	    </div>
            </div>
          </div>
          </div>
        </div>
    </div>
  </div>
  
  <?php include('layout/dashboard/profile.php'); ?>
</div>


<div class="modal inactive" id="chat" data-backdrop="false">
  <div class="right w-xxl grey lt b-l">
    <div ui-include="'../views/blocks/modal.chat.html'"></div>
  </div>
</div>

<!-- ############ PAGE END-->

<!-- FOOTER -->
<div class="p-a white lt text-grey">
  <div class="row">
    <div class="col-sm-12">
      <center>
        Copyright &copy; 2019 <i>GUD</i><b>TRAK</b> &nbsp; | &nbsp;  All Rights Reserved
      </center>
    </div>
  </div>
</div>
<!-- FOOTER -->

<!-- Modal -->
  <div class="modal fade clearfix modal-z-index" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            <h3 class="modal_title"></h3>
            <p>&nbsp;</p>
            <div class="table-responsive">
              <div class="content-modal"></div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


  <div class="modal fade clearfix modal-z-index" id="myProfile" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <p>
            <div class="box huf-modal">
              <div class="p-a-md text-center">
                <p><img src="<?php echo base_url()?>assets/dashboard/assets/images/a1.jpg" class="img-circle w-xs"></p>
                <a href class="text-md block">Adhitia Panji Kusumowinoto</a>
                <p><small>Jakarta Utara, Tanjung Priok</small></p>
                <div>
                  <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                  <i class="fa fa-facebook"></i>
                  <i class="fa fa-facebook indigo"></i>
                </a>
                <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                  <i class="fa fa-twitter"></i>
                  <i class="fa fa-twitter light-blue"></i>
                </a>
                <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                  <i class="fa fa-google-plus"></i>
                  <i class="fa fa-google-plus red"></i>
                </a>
                </div>
                <div class="text-center m-t">
                  <a href class="btn btn-sm rounded danger w-sm text-putih" data-dismiss="modal">Tutup</a>
                </div>
              </div>
          </p>
        </div>
      </div>
      
    </div>
  </div>
<!-- Modal -->

    </div>
  </div>
  <!-- / content -->

<!-- ############ LAYOUT END-->

  </div>
  
            <div class="modal fade" id="user2" >
              <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
                 
                  <form id='user_form' name='user_form' method="POST" role="form" enctype="multipart/form-data" autocomplete="off" novalidate>
                  <div class="box box-danger">
                    <div class="box-header">
                      <h3 class="box-title">User Form</h3>
                    </div>
                    
                    <div class="form-group">
                      <label>User Name:</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-user"></i>
                        </div>
                        <input type="text" class="form-control" id="username" name="username" required>
                      </div>
                      <label>Password :</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-lock"></i>
                        </div>
                        <input type="password" class="form-control" id="password" name="password" required>
                      </div>
                      <label>Name:</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-user"></i>
                        </div>
                        <input type="text" class="form-control" id="name" name="name" required>
                      </div>
                      
                      <label>Email:</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                        </div>
                        <input type="email" class="form-control" id="email" name="email" required>
                      </div>
                      
                      <label>Phone:</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                        </div>
                        <input type="text" class="form-control" id="phone" name="phone" required>
                      </div>
                      
                      <label>Information:</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-edit"></i>
                        </div>
                        <textarea name="memo" id="memo"  class="form-control" id="memo"></textarea>
                      </div>
                      
                      <label>Dashboard Team:</label>

                      <div class="input-group">
                       
                       <input type="checkbox" value="1" name="team" id="team"> Dashboard Team
                      </div>
                      
                      
                      
                      <label>Level :</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-retweet"></i>
                        </div>
                        <select class="form-control" name="level" id="level">
                          <option value='4'>Viewer</option>
                          <option value='3'>Author</option>
                          <option value='2'>Editor</option>
                          <option value='1'>Administrator</option>
                        </select>
                      </div>
                      
                      <label>Status :</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-retweet"></i>
                        </div>
                        <select class="form-control" name="is_active" id="is_active">
                          <option selected="0">--Choose--</option>
                          <option value='2'>Not Active</option>
                          <option value='1'>Active</option>
                          
                        </select>
                      </div>
                      
                      <div id="uploadFile_div" class="form-group">
                        <label for="exampleInputFile">Icon Avatar</label>
                        <input type="file" id="photo" name="photo">
                         <input type="hidden" class="form-control" id="photo1" name="photo1">
                         <input type="hidden" class="form-control" id="flagurl" name="flagurl">
                         <p id="photoinfo">Upload your photo.</p>
                      </div>
                      
                        
                    </div>
                    <div class="form-group">
                       <button class="btn btn-success" id="submit" onclick='simpan()'>submit</button>
                       
                    </div>
                    
                
                  </div>
                  
                  <?php echo form_close();?>  
                    
                </div>
                  
                    
                  
              </div>    
              </div>
            </div>
            
<!-- build:js scripts/app.html.js -->
<!-- jQuery -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/tether/dist/js/tether.min.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/underscore/underscore-min.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/PACE/pace.min.js"></script>

  <script src="<?php echo base_url()?>assets/dashboard/scripts/config.lazyload.js"></script>

  <script src="<?php echo base_url()?>assets/dashboard/scripts/palette.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-load.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-jp.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-include.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-device.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-form.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-nav.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-screenfull.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-scroll-to.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-toggle-class.js"></script>

  <script src="<?php echo base_url()?>assets/dashboard/scripts/app.js"></script>

  <!-- ajax -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/jquery-pjax/jquery.pjax.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ajax.js"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  
  <!-- <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script> -->
  
  <script src="<?php echo base_url()?>assets/js/jquery.blockui.min.js" type="text/javascript"></script>


  <script type="text/javascript">
    $(document).ready(function() {
      // alert('test');
         $('.datatable').DataTable();
    } );

    function open_modal_1(param=null)
    {

      $.blockUI({ message: '<span style="font-weight:bold;">Permintaan Anda Sedang Diproses...</span>' });

      var title = '';
      var url   = '';
      
      if(param==1){
        title = '<center>Detail List Truck</center>';
        func  = 'list_truck';
      }else if(param==2){
        title = '<center>Detail List Idle Truck</center>';
        func  = 'list_idle_truck';
      }else if(param==3){
        title = '<center>Detail List Truck OTR</center>';
        func  = 'list_truck_otr';
      }else if(param==4){
        title = '<center>Detail List Driver</center>';
        func  = 'list_driver';
      }else if(param==5){
        title = '<center>Detail List Ticket</center>';
        func  = 'list_ticket';
      }else if(param==6){
        title = '<center>Detail List Karnets</center>';
        func  = 'list_karnets';
      }else if(param==7){
        title = '<center>Detail List Asuransi</center>';
        func  = 'list_asuransi';
      }

      $('.modal_title').html(title);

      url   = '<?=base_url()?>dashboard/get_data_via_ajax';
      $.ajax({
        url : url,
        type: "POST",
        data: {"func":func},
        success: function(data)
        {
           $('#myModal').modal('show');
           $('.modal_title').html(title);
           $('.content-modal').html(data);
           $.unblockUI();
         },
         error: function (jqXHR, textStatus, errorThrown)
         {
           alert('Error adding / update data');
           $.unblockUI();
         }
      });
    }

    function open_modal_2()
    {
         $('#myProfile').modal('show');
    }


  </script>
<!-- endbuild -->
</body>
</html>
