<?php include('layout/dashboard/header.php'); ?>

<?php include('layout/dashboard/nav.php'); ?>

<!-- ############ PAGE START-->
<div class="row-col b-b body-margin-top">
  <div class="col-md">
    <div class="padding">
      <div class="row">
        <div class="col-sm-12">
          <div class="box">
            <div class="box-header">
              <h1 class="h1-custom"><i class="fa fa-database" aria-hidden="true"></i> Database</h1>
              <small>Data-data truk yang sudah terdaftarkan</small>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <h3><?=$caption?></h3>
                    <br>
                    <div class="table-responsive">
                      <?php generate_datatable_api($list_data); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include('layout/dashboard/profile.php'); ?>
</div>


<div class="modal inactive" id="chat" data-backdrop="false">
  <div class="right w-xxl grey lt b-l">
    <div ui-include="'../views/blocks/modal.chat.html'"></div>
  </div>
</div>

<!-- ############ PAGE END-->

<!-- FOOTER -->
<div class="p-a white lt text-grey">
  <div class="row">
    <div class="col-sm-12">
      <center>
        Copyright &copy; 2019 <i>GUD</i><b>TRAK</b> &nbsp; | &nbsp;  All Rights Reserved
      </center>
    </div>
  </div>
</div>
<!-- FOOTER -->

<!-- Modal -->
  <div class="modal fade clearfix modal-z-index" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            <h3 class="modal_title"></h3>
            <p>&nbsp;</p>
            <div class="table-responsive">
              <div class="content-modal"></div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


  <div class="modal fade clearfix modal-z-index" id="myProfile" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <p>
            <div class="box huf-modal">
              <div class="p-a-md text-center">
                <p><img src="<?php echo base_url()?>assets/dashboard/assets/images/a1.jpg" class="img-circle w-xs"></p>
                <a href class="text-md block">Adhitia Panji Kusumowinoto</a>
                <p><small>Jakarta Utara, Tanjung Priok</small></p>
                <div>
                  <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                  <i class="fa fa-facebook"></i>
                  <i class="fa fa-facebook indigo"></i>
                </a>
                <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                  <i class="fa fa-twitter"></i>
                  <i class="fa fa-twitter light-blue"></i>
                </a>
                <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                  <i class="fa fa-google-plus"></i>
                  <i class="fa fa-google-plus red"></i>
                </a>
                </div>
                <div class="text-center m-t">
                  <a href class="btn btn-sm rounded danger w-sm text-putih" data-dismiss="modal">Tutup</a>
                </div>
              </div>
          </p>
        </div>
      </div>
      
    </div>
  </div>
<!-- Modal -->

    </div>
  </div>
  <!-- / content -->

<!-- ############ LAYOUT END-->

  </div>
  
  <div class="modal fade" id="user2" >
    <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
       
        <form id='user_form' name='user_form' method="POST" role="form" enctype="multipart/form-data" autocomplete="off" novalidate>
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">User Form</h3>
          </div>
          
          <div class="form-group">
            <label>User Name:</label>

            <div class="input-group">
              <div class="input-group-addon">
              <i class="fa fa-user"></i>
              </div>
              <input type="text" class="form-control" id="username" name="username" required>
            </div>
            <label>Password :</label>

            <div class="input-group">
              <div class="input-group-addon">
              <i class="fa fa-lock"></i>
              </div>
              <input type="password" class="form-control" id="password" name="password" required>
            </div>
            <label>Name:</label>

            <div class="input-group">
              <div class="input-group-addon">
              <i class="fa fa-user"></i>
              </div>
              <input type="text" class="form-control" id="name" name="name" required>
            </div>
            
            <label>Email:</label>

            <div class="input-group">
              <div class="input-group-addon">
              <i class="fa fa-envelope"></i>
              </div>
              <input type="email" class="form-control" id="email" name="email" required>
            </div>
            
            <label>Phone:</label>

            <div class="input-group">
              <div class="input-group-addon">
              <i class="fa fa-phone"></i>
              </div>
              <input type="text" class="form-control" id="phone" name="phone" required>
            </div>
            
            <label>Information:</label>

            <div class="input-group">
              <div class="input-group-addon">
              <i class="fa fa-edit"></i>
              </div>
              <textarea name="memo" id="memo"  class="form-control" id="memo"></textarea>
            </div>
            
            <label>Dashboard Team:</label>

            <div class="input-group">
             
             <input type="checkbox" value="1" name="team" id="team"> Dashboard Team
            </div>
            
            
            
            <label>Level :</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-retweet"></i>
              </div>
              <select class="form-control" name="level" id="level">
                <option value='4'>Viewer</option>
                <option value='3'>Author</option>
                <option value='2'>Editor</option>
                <option value='1'>Administrator</option>
              </select>
            </div>
            
            <label>Status :</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-retweet"></i>
              </div>
              <select class="form-control" name="is_active" id="is_active">
                <option selected="0">--Choose--</option>
                <option value='2'>Not Active</option>
                <option value='1'>Active</option>
                
              </select>
            </div>
            
            <div id="uploadFile_div" class="form-group">
              <label for="exampleInputFile">Icon Avatar</label>
              <input type="file" id="photo" name="photo">
               <input type="hidden" class="form-control" id="photo1" name="photo1">
               <input type="hidden" class="form-control" id="flagurl" name="flagurl">
               <p id="photoinfo">Upload your photo.</p>
            </div>
            
              
          </div>
          <div class="form-group">
             <button class="btn btn-success" id="submit" onclick='simpan()'>submit</button>
             
          </div>
          
      
        </div>
        
        <?php echo form_close();?>  
          
      </div>
        
          
        
    </div>    
    </div>
  </div>
            
<!-- build:js scripts/app.html.js -->
<!-- jQuery -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/tether/dist/js/tether.min.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/underscore/underscore-min.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/PACE/pace.min.js"></script>

  <script src="<?php echo base_url()?>assets/dashboard/scripts/config.lazyload.js"></script>

  <script src="<?php echo base_url()?>assets/dashboard/scripts/palette.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-load.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-jp.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-include.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-device.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-form.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-nav.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-screenfull.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-scroll-to.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-toggle-class.js"></script>

  <script src="<?php echo base_url()?>assets/dashboard/scripts/app.js"></script>

  <!-- ajax -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/jquery-pjax/jquery.pjax.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ajax.js"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  
  <!-- <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script> -->
  
  <script src="<?php echo base_url()?>assets/js/jquery.blockui.min.js" type="text/javascript"></script>


  <script type="text/javascript">
    $(document).ready(function() {
      // alert('test');
         $('.datatable').DataTable();
    } );

    function open_modal_2()
    {
         $('#myProfile').modal('show');
    }



  </script>
<!-- endbuild -->
</body>
</html>
