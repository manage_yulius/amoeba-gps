<!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/main_menu/images/icong.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="<?php echo base_url()?>assets/dashboard/assets/images/logo.png">
  
  <!-- style -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/dashboard/assets/animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url()?>assets/dashboard/assets/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url()?>assets/dashboard/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url()?>assets/dashboard/assets/material-design-icons/material-design-icons.css" type="text/css" />

  <link rel="stylesheet" href="<?php echo base_url()?>assets/dashboard/assets/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
  <!-- build:css ../assets/styles/app.min.css -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/dashboard/assets/styles/app.css" type="text/css" />

  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" /> -->
  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css" /> -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href=""https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.bootstrap.min.css>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">



  <!-- endbuild -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/dashboard/assets/styles/font.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/tambahan.css">