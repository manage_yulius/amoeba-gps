<div class="col-md w-lg w-auto-sm light lt profil">
    <div class="box huf">
      <div class="p-a-md text-center ungu">
        <ps style="text-transform: capitalize;"><img src="<?php echo base_url()?>assets/dashboard/assets/images/a1.jpg" class="img-circle w-xs"></p>
        <a href class="text-md block text-putih"><?=$this->session->userdata('username')?></a>
        <p><small>Jakarta Utara, Tanjung Priok</small></p>
        <div>
          <a href="" class="btn btn-icon btn-social rounded white btn-sm">
          <i class="fa fa-facebook text-ungu"></i>
          <i class="fa fa-facebook indigo"></i>
        </a>
        <a href="" class="btn btn-icon btn-social rounded white btn-sm">
          <i class="fa fa-twitter text-ungu"></i>
          <i class="fa fa-twitter light-blue"></i>
        </a>
        <a href="" class="btn btn-icon btn-social rounded white btn-sm">
          <i class="fa fa-google-plus text-ungu"></i>
          <i class="fa fa-google-plus red"></i>
        </a>
        </div>
        <div class="text-center m-t">
          <a href="#" onclick="open_modal_2()" title="Detail User" class="btn btn-sm rounded primary w-sm m-y-xs text-putih">Detail</a>
          <a href="<?=base_url()?>dashboard/logout" class="btn btn-sm rounded danger w-sm text-putih">Keluar</a>
        </div>
      </div>
      <p>&nbsp;</p>
      <div class="p-a">
        <h3 class="company">Company Detail</h3>
        <table class="h4">
          <tr>
            <td><i class="fa fa-building" aria-hidden="true"></i> Perusahaan</td>
            <td width="10" align="center">:</td>
            <td><?=$this->session->userdata('nama_lengkap')?></td>
          </tr>
          <tr>
            <td><i class="fa fa-building" aria-hidden="true"></i> Tipe Perusahaan</td>
            <td>:</td>
            <td>Terbuka (Tbk.)</td>
          </tr>
          <tr>
            <td><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;Alamat</td>
            <td>:</td>
            <td>Jakarta Utara</td>
          </tr>
          <tr>
            <td><i class="fa fa-phone" aria-hidden="true"></i> Telepon</td>
            <td>:</td>
            <td>08123456789</td>
          </tr>
          <tr>
            <td><i class="fa fa-envelope-o" aria-hidden="true"></i> E-mail</td>
            <td>:</td>
            <td>info@adhitia.unch</td>
          </tr>
          <tr>
            <td><i class="fa fa-credit-card" aria-hidden="true"></i> NPWP</td>
            <td>:</td>
            <td>001122334455</td>
          </tr>
        </table>
      </div>
      <div class="p-a">
        <h3 class="company">Pool</h3>
        <table class="h4">
          <tr>
            <td><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;Alamat Pool</td>
            <td>:</td>
            <td>Jakarta Utara</td>
          </tr>
          <tr>
            <td><i class="fa fa-phone" aria-hidden="true"></i> Telepon</td>
            <td>:</td>
            <td>08123456789</td>
          </tr>
          <tr>
            <td><i class="fa fa-envelope-o" aria-hidden="true"></i> FAX</td>
            <td>:</td>
            <td>021-2223424</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>