<div class="p-a white lt box-shadow bayangan2">
  <div class="row">
    <div class="col-sm-12 padding-bayangan2">
      <center>
        <div class="row-menu-nav">
          <div class="menu-nav">
            <a href="#" id="dashboard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu" aria-labelledby="dashboard">
              <li><span style="margin-left: 20px;"><b>Summary</b></span></li>
              <li role="separator" class="divider"></li>              
              <li><a href="<?=base_url()?>dashboard/guddata"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>
              <li><a href="#"><i class="fa fa-area-chart" aria-hidden="true"></i> D-3</a></li>
              <li><a href="#"><i class="fa fa-area-chart" aria-hidden="true"></i> D-6</a></li>
              <li><a href="#"><i class="fa fa-area-chart" aria-hidden="true"></i> D-9</a></li>
            </ul>
          </div>
          <div class="menu-nav">
            <a href="#" id="database" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-database" aria-hidden="true"></i> Database <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu" aria-labelledby="database">
              <li><span style="margin-left: 20px;"><b>Data</b></span></li>
              <li role="separator" class="divider"></li>
              <li><a href="<?=base_url()?>database/truck"><i class="fa fa-truck" aria-hidden="true"></i> Truck</a></li>
              <li><a href="<?=base_url()?>database/driver"><i class="fa fa-user" aria-hidden="true"></i> Driver</a></li>
              <li><a href="<?=base_url()?>database/asuransi"><i class="fa fa-credit-card" aria-hidden="true"></i> Asuransi</a></li>
            </ul>
          </div>
          <div class="menu-nav">
            <a href="#" id="register" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Register <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu" aria-labelledby="register">
              <li><span style="margin-left: 20px;"><b>Data</b></span></li>
              <li role="separator" class="divider"></li>
              <li><a href="<?php echo base_url()?>auth/register"><i class="fa fa-truck" aria-hidden="true"></i> Truck</a></li>
              <li><a href="<?php echo base_url()?>auth/driver"><i class="fa fa-user" aria-hidden="true"></i> Driver</a></li>
              <li><a href="<?php echo base_url()?>auth/kernet"><i class="fa fa-male" aria-hidden="true"></i> Kernet</a></li>
              <li><a href="<?=base_url()?>auth/upload"><i class="fa fa-upload" aria-hidden="true"></i> Upload</a></li>
            </ul>
          </div>
          <div class="menu-nav">
            <a href="<?php echo base_url()?>auth/forum"><i class="fa fa-users" aria-hidden="true"></i> Forum</a>
          </div>
        </div>          
      </center>
  </div>
  </div>
</div>
<div class="p-a white lt box-shadow bayangan3">
  <div class="row">
    <div class="col-sm-12 padding-bayangan3">
      <center>
        <div class="row-menu-nav">
          <div class="menu-nav">
            <a href="#" id="dashboard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-tachometer" aria-hidden="true"></i> <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu" aria-labelledby="dashboard">
              <li><span style="margin-left: 20px;"><b>Summary</b></span></li>
              <li role="separator" class="divider"></li>              
              <li><a href="<?=base_url()?>dashboard/guddata"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>
              <li><a href="#"><i class="fa fa-area-chart" aria-hidden="true"></i> D-3</a></li>
              <li><a href="#"><i class="fa fa-area-chart" aria-hidden="true"></i> D-6</a></li>
              <li><a href="#"><i class="fa fa-area-chart" aria-hidden="true"></i> D-9</a></li>
            </ul>
          </div>
          <div class="menu-nav">
            <a href="#" id="database" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-database" aria-hidden="true"></i> <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu" aria-labelledby="database">
              <li><span style="margin-left: 20px;"><b>Data</b></span></li>
              <li role="separator" class="divider"></li>
              <li><a href="<?=base_url()?>database/truck"><i class="fa fa-truck" aria-hidden="true"></i> Truck</a></li>
              <li><a href="<?=base_url()?>database/driver"><i class="fa fa-user" aria-hidden="true"></i> Driver</a></li>
              <li><a href="<?=base_url()?>database/asuransi"><i class="fa fa-credit-card" aria-hidden="true"></i> Asuransi</a></li>
            </ul>
          </div>
          <div class="menu-nav">
            <a href="#" id="register" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-pencil-square-o" aria-hidden="true"></i> <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu" aria-labelledby="register">
              <li><span style="margin-left: 20px;"><b>Data</b></span></li>
              <li role="separator" class="divider"></li>
              <li><a href="<?php echo base_url()?>auth/register"><i class="fa fa-truck" aria-hidden="true"></i> Truck</a></li>
              <li><a href="<?php echo base_url()?>auth/driver"><i class="fa fa-user" aria-hidden="true"></i> Driver</a></li>
              <li><a href="<?php echo base_url()?>auth/kernet"><i class="fa fa-male" aria-hidden="true"></i> Kernet</a></li>
              <li><a href="<?=base_url()?>auth/upload"><i class="fa fa-upload" aria-hidden="true"></i> Upload</a></li>
            </ul>
          </div>
          <div class="menu-nav">
            <a href="<?php echo base_url()?>auth/forum">
              <i class="fa fa-users" aria-hidden="true"></i>
            </a>
          </div>
        </div>          
      </center>
  </div>
  </div>
</div>