<!--  favicon -->
<link rel="shortcut icon" href="<?php echo base_url()?>assets/main_menu/images/icong.png">
<!--  apple-touch-icon -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url()?>assets/main_menu/assets/img/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url()?>assets/main_menu/assets/img/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url()?>assets/main_menu/assets/img/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url()?>assets/main_menu/assets/img/ico/apple-touch-icon-57-precomposed.png"> 

<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>
<!-- FontAwesome CSS -->
<link href="<?php echo base_url()?>assets/main_menu/assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- Material Icons CSS -->
<link href="<?php echo base_url()?>assets/main_menu/assets/fonts/iconfont/material-icons.css" rel="stylesheet">
<!-- magnific-popup -->
<link href="<?php echo base_url()?>assets/main_menu/assets/magnific-popup/magnific-popup.css" rel="stylesheet">
<!-- owl.carousel -->
<link href="<?php echo base_url()?>assets/main_menu/assets/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/main_menu/assets/owl.carousel/assets/owl.theme.default.min.css" rel="stylesheet">
<!-- flexslider -->
<link href="<?php echo base_url()?>assets/main_menu/assets/flexSlider/flexslider.css" rel="stylesheet">
<!-- materialize -->
<link href="<?php echo base_url()?>assets/main_menu/assets/materialize/css/materialize.min.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="<?php echo base_url()?>assets/main_menu/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- shortcodes -->
<link href="<?php echo base_url()?>assets/main_menu/assets/css/shortcodes/shortcodes.css" rel="stylesheet">
<!-- Style CSS -->
<link href="<?php echo base_url()?>assets/main_menu/style.css" rel="stylesheet">

<!-- RS5.0 Main Stylesheet -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/main_menu/assets/revolution/css/settings.css">
<!-- RS5.0 Layers and Navigation Styles -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/main_menu/assets/revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/main_menu/assets/revolution/css/navigation.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="<?php echo base_url()?>assets/main_menu/https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="<?php echo base_url()?>assets/main_menu/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->