<?php include('layout/dashboard/header.php'); ?>

 <?php include('layout/dashboard/nav.php'); ?>

<!-- ############ PAGE START-->
<div class="row-col b-b body-margin-top">
  <div class="col-md">
    <div class="padding">
        <div class="row">         
          <div class="col-sm-12">
            <div class="box noback-row">
              <center>
                <div class="menu">
                  <a href="#" onclick="open_modal_1(1)" title="Total Truck" class="list-group-item text-ellipsis">
                    <img src="<?php echo base_url()?>assets/dashboard/assets/images/a2.png" class="wd-50 m-r-sm img-circle wik">
                    <p><h3><?=($total_kendaraan[0]->total)?></h3> Total Trucks</p>
                  </a>
                </div>                
                <div class="menu">
                  <a href="#" onclick="open_modal_1(2)" title="Total Truck" class="list-group-item text-ellipsis">
                    <img src="<?php echo base_url()?>assets/dashboard/assets/images/a3.png" class="wd-50 m-r-sm img-circle wik">
                    <p><h3>13</h3> Idle Trucks</p>
                  </a>
                </div>
                <div class="menu">
                  <a href="#" onclick="open_modal_1(3)" title="Total Truck" class="list-group-item text-ellipsis">
                    <img src="<?php echo base_url()?>assets/dashboard/assets/images/a4.png" class="wd-50 m-r-sm img-circle wik">
                    <p><h3>7</h3> Trucks OTR</p>
                  </a>
                </div>
                <div class="menu">
                  <a href="#" onclick="open_modal_1(4)" title="Total Truck" class="list-group-item text-ellipsis">
                    <img src="<?php echo base_url()?>assets/dashboard/assets/images/a5.png" class="wd-50 m-r-sm img-circle wik">
                    <p><h3><?=($total_supir[0]->total)?></h3> Total Drivers</p>
                  </a>
                </div>
                <div class="menu">
                  <a href="#" onclick="open_modal_1(5)" title="Total Truck" class="list-group-item text-ellipsis">
                    <img src="<?php echo base_url()?>assets/dashboard/assets/images/a6.png" class="wd-50 m-r-sm img-circle wik">
                    <p><h3>14</h3> Tickets</p>
                  </a>
                </div>
                <div class="menu">
                  <a href="#" onclick="open_modal_1(6)" title="Total Truck" class="list-group-item text-ellipsis">
                    <img src="<?php echo base_url()?>assets/dashboard/assets/images/a7.png" class="wd-50 m-r-sm img-circle wik">
                    <p><h3><?=($total_kernet[0]->total)?></h3> Total Karnets</p>
                  </a>
                </div>
                <div class="menu">
                  <a href="#" onclick="open_modal_1(7)" title="Total Truck" class="list-group-item text-ellipsis">
                    <img src="<?php echo base_url()?>assets/dashboard/assets/images/a8.png" class="wd-50 m-r-sm img-circle wik">
                    <p><h3><?=($total_asuransi[0]->total)?></h3> Asuransi</p>
                  </a>
                </div>
              </center>
            </div>
          </div>
        </div>
        <div class="row">         
          <div class="col-sm-12">
            <div class="box">
            <div class="box-header">
              <h2>Expired Data</h2>
              <?php #debux($expired); ?>
            </div>
              <div class="box-body">
                <div class="row row-sm">
                  <div class="col-sm-4 col-tambahan">
                    <b><h5>Under 3 Months</h5></b>
                    KIR (6)
                    <div class="progress">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:60%"></div>
                  </div>
                  SIM (<?=$expired['3bulan']['angka']?>)
                    <div class="progress">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?=$expired['3bulan']['persen']?>%"></div>
                   </div>
                  STNK (7)
                    <div class="progress">
                      <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                  </div>
                  </div>
                  <div class="col-sm-4 col-tambahan">
                    <b><h5>Under 6 Months</h5></b>
                    KIR (6)
                    <div class="progress">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:60%"></div>
                  </div>
                  SIM (<?=$expired['6bulan']['angka']?>)
                    <div class="progress">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?=$expired['6bulan']['persen']?>%"></div>
                   </div>
                  STNK (7)
                    <div class="progress">
                      <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                  </div>
                  </div>
                  <div class="col-sm-4 col-tambahan">
                    <b><h5>Under 9 Months</h5></b>
                    KIR (6)
                    <div class="progress">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:60%"></div>
                  </div>
                  SIM (<?=$expired['9bulan']['angka']?>)
                    <div class="progress">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?=$expired['9bulan']['persen']?>%"></div>
                   </div>
                  STNK (7)
                    <div class="progress">
                      <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:70%"></div>
                  </div>
                  </div>
                </div>
              </div>
          </div>
          </div>
        </div>
      <div class="row nogutter box margin-tambahan">
        <div class="col-sm-4">
          <div class="box-header">
            <h2>Trucks <i class="fa fa-truck" aria-hidden="true"></i> On The Road</h2>
          </div>
          <div class="box-body">
            <div class="streamline b-l m-l-md">
              <div class="sl-item">
                <div class="sl-left">
                  <img src="<?php echo base_url()?>assets/dashboard/assets/images/a1.jpg" class="img-circle">
                </div>
                <div class="sl-content">
                  <div class="sl-date text-muted">Driver ID 981230110</div>
                  <div class="sl-author">
                    <a>Adhitia Panji</a>
                  </div>
                  <div class="sl-footer">
                      <span class="text-biru"><i class="fa fa-fw fa-truck text-muted"></i> B 1544 KLI</span>
                      &nbsp;&nbsp;
                      <span class="text-pink"><i class="fa fa-fw fa-ticket text-muted"></i> 0219318451</span>
                  </div>
                </div>
              </div>                  
            </div>

            <div class="streamline b-l m-l-md">
              <div class="sl-item">
                <div class="sl-left">
                  <img src="<?php echo base_url()?>assets/dashboard/assets/images/a1.jpg" class="img-circle">
                </div>
                <div class="sl-content">
                  <div class="sl-date text-muted">Driver ID 981230110</div>
                  <div class="sl-author">
                    <a>Adhitia Panji</a>
                  </div>
                  <div class="sl-footer">
                      <span class="text-biru"><i class="fa fa-fw fa-truck text-muted"></i> B 1544 KLI</span>
                      &nbsp;&nbsp;
                      <span class="text-pink"><i class="fa fa-fw fa-ticket text-muted"></i> 0219318451</span>
                  </div>
                </div>
              </div>                  
            </div>

            <div class="streamline b-l m-l-md">
              <div class="sl-item">
                <div class="sl-left">
                  <img src="<?php echo base_url()?>assets/dashboard/assets/images/a1.jpg" class="img-circle">
                </div>
                <div class="sl-content">
                  <div class="sl-date text-muted">Driver ID 981230110</div>
                  <div class="sl-author">
                    <a>Adhitia Panji</a>
                  </div>
                  <div class="sl-footer">
                      <span class="text-biru"><i class="fa fa-fw fa-truck text-muted"></i> B 1544 KLI</span>
                      &nbsp;&nbsp;
                      <span class="text-pink"><i class="fa fa-fw fa-ticket text-muted"></i> 0219318451</span>
                  </div>
                </div>
              </div>                  
            </div>

            <div class="streamline b-l m-l-md">
              <div class="sl-item">
                <div class="sl-left">
                  <img src="<?php echo base_url()?>assets/dashboard/assets/images/a1.jpg" class="img-circle">
                </div>
                <div class="sl-content">
                  <div class="sl-date text-muted">Driver ID 981230110</div>
                  <div class="sl-author">
                    <a>Adhitia Panji</a>
                  </div>
                  <div class="sl-footer">
                      <span class="text-biru"><i class="fa fa-fw fa-truck text-muted"></i> B 1544 KLI</span>
                      &nbsp;&nbsp;
                      <span class="text-pink"><i class="fa fa-fw fa-ticket text-muted"></i> 0219318451</span>
                  </div>
                </div>
              </div>                  
            </div>
          </div>

          </div>
          <div class="col-sm-8 ungu lt">
            <div class="box-header">
              <h2>Truck Location</h2>
            </div>
            <div class="box-body">
              <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15868.375843168884!2d106.8945571!3d-6.1180531!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sid!2sid!4v1549798536862" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
    </div>
  </div>

  <!-- PROFILE -->
  <?php include('layout/dashboard/profile.php'); ?>
  <!-- END PROFILE -->

<div class="modal inactive" id="chat" data-backdrop="false">
  <div class="right w-xxl grey lt b-l">
    <div ui-include="'../views/blocks/modal.chat.html'"></div>
  </div>
</div>

<!-- ############ PAGE END-->

<!-- FOOTER -->
<div class="p-a white lt text-grey">
  <div class="row">
    <div class="col-sm-12">
      <center>
        Copyright &copy; 2019 <i>GUD</i><b>TRAK</b> &nbsp; | &nbsp;  All Rights Reserved
      </center>
    </div>
  </div>
</div>
<!-- FOOTER -->

<!-- Modal -->
  <div class="modal fade clearfix modal-z-index" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
            <h3 class="modal_title"></h3>
            <p>&nbsp;</p>
            <div class="table-responsive">
              <div class="content-modal"></div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


  <div class="modal fade clearfix modal-z-index" id="myProfile" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <p>
            <div class="box huf-modal">
              <div class="p-a-md text-center">
                <p><img src="<?php echo base_url()?>assets/dashboard/assets/images/a1.jpg" class="img-circle w-xs"></p>
                <a href class="text-md block">Adhitia Panji Kusumowinoto</a>
                <p><small>Jakarta Utara, Tanjung Priok</small></p>
                <div>
                  <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                  <i class="fa fa-facebook"></i>
                  <i class="fa fa-facebook indigo"></i>
                </a>
                <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                  <i class="fa fa-twitter"></i>
                  <i class="fa fa-twitter light-blue"></i>
                </a>
                <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                  <i class="fa fa-google-plus"></i>
                  <i class="fa fa-google-plus red"></i>
                </a>
                </div>
                <div class="text-center m-t">
                  <a href class="btn btn-sm rounded danger w-sm text-putih" data-dismiss="modal">Tutup</a>
                </div>
              </div>
          </p>
        </div>
      </div>
      
    </div>
  </div>
<!-- Modal -->

    </div>
  </div>
  <!-- / content -->

<!-- ############ LAYOUT END-->

  </div>
  
            <div class="modal fade" id="user2" >
              <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
                 
                  <form id='user_form' name='user_form' method="POST" role="form" enctype="multipart/form-data" autocomplete="off" novalidate>
                  <div class="box box-danger">
                    <div class="box-header">
                      <h3 class="box-title">User Form</h3>
                    </div>
                    
                    <div class="form-group">
                      <label>User Name:</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-user"></i>
                        </div>
                        <input type="text" class="form-control" id="username" name="username" required>
                      </div>
                      <label>Password :</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-lock"></i>
                        </div>
                        <input type="password" class="form-control" id="password" name="password" required>
                      </div>
                      <label>Name:</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-user"></i>
                        </div>
                        <input type="text" class="form-control" id="name" name="name" required>
                      </div>
                      
                      <label>Email:</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                        </div>
                        <input type="email" class="form-control" id="email" name="email" required>
                      </div>
                      
                      <label>Phone:</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                        </div>
                        <input type="text" class="form-control" id="phone" name="phone" required>
                      </div>
                      
                      <label>Information:</label>

                      <div class="input-group">
                        <div class="input-group-addon">
                        <i class="fa fa-edit"></i>
                        </div>
                        <textarea name="memo" id="memo"  class="form-control" id="memo"></textarea>
                      </div>
                      
                      <label>Dashboard Team:</label>

                      <div class="input-group">
                       
                       <input type="checkbox" value="1" name="team" id="team"> Dashboard Team
                      </div>
                      
                      
                      
                      <label>Level :</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-retweet"></i>
                        </div>
                        <select class="form-control" name="level" id="level">
                          <option value='4'>Viewer</option>
                          <option value='3'>Author</option>
                          <option value='2'>Editor</option>
                          <option value='1'>Administrator</option>
                        </select>
                      </div>
                      
                      <label>Status :</label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-retweet"></i>
                        </div>
                        <select class="form-control" name="is_active" id="is_active">
                          <option selected="0">--Choose--</option>
                          <option value='2'>Not Active</option>
                          <option value='1'>Active</option>
                          
                        </select>
                      </div>
                      
                      <div id="uploadFile_div" class="form-group">
                        <label for="exampleInputFile">Icon Avatar</label>
                        <input type="file" id="photo" name="photo">
                         <input type="hidden" class="form-control" id="photo1" name="photo1">
                         <input type="hidden" class="form-control" id="flagurl" name="flagurl">
                         <p id="photoinfo">Upload your photo.</p>
                      </div>
                      
                        
                    </div>
                    <div class="form-group">
                       <button class="btn btn-success" id="submit" onclick='simpan()'>submit</button>
                       
                    </div>
                    
                
                  </div>
                  
                  <?php echo form_close();?>  
                    
                </div>
                  
                    
                  
              </div>    
              </div>
            </div>
            
<!-- build:js scripts/app.html.js -->
<!-- jQuery -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/tether/dist/js/tether.min.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/underscore/underscore-min.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/PACE/pace.min.js"></script>

  <script src="<?php echo base_url()?>assets/dashboard/scripts/config.lazyload.js"></script>

  <script src="<?php echo base_url()?>assets/dashboard/scripts/palette.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-load.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-jp.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-include.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-device.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-form.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-nav.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-screenfull.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-scroll-to.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ui-toggle-class.js"></script>

  <script src="<?php echo base_url()?>assets/dashboard/scripts/app.js"></script>

  <!-- ajax -->
  <script src="<?php echo base_url()?>assets/dashboard/libs/jquery/jquery-pjax/jquery.pjax.js"></script>
  <script src="<?php echo base_url()?>assets/dashboard/scripts/ajax.js"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  
  <!-- <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script> -->
  
  <script src="<?php echo base_url()?>assets/js/jquery.blockui.min.js" type="text/javascript"></script>


  <script type="text/javascript">
    $(document).ready(function() {
      // alert('test');
         $('.datatable').DataTable();
    } );

    function open_modal_1(param=null)
    {

      $.blockUI({ message: '<span style="font-weight:bold;">Permintaan Anda Sedang Diproses...</span>' });

      var title = '';
      var url   = '';
      
      if(param==1){
        title = '<center>Detail List Truck</center>';
        func  = 'list_truck';
      }else if(param==2){
        title = '<center>Detail List Idle Truck</center>';
        func  = 'list_idle_truck';
      }else if(param==3){
        title = '<center>Detail List Truck OTR</center>';
        func  = 'list_truck_otr';
      }else if(param==4){
        title = '<center>Detail List Driver</center>';
        func  = 'list_driver';
      }else if(param==5){
        title = '<center>Detail List Ticket</center>';
        func  = 'list_ticket';
      }else if(param==6){
        title = '<center>Detail List Karnets</center>';
        func  = 'list_karnets';
      }else if(param==7){
        title = '<center>Detail List Asuransi</center>';
        func  = 'list_asuransi';
      }

      $('.modal_title').html(title);

      url   = '<?=base_url()?>dashboard/get_data_via_ajax';
      $.ajax({
        url : url,
        type: "POST",
        data: {"func":func},
        success: function(data)
        {
           $('#myModal').modal('show');
           $('.modal_title').html(title);
           $('.content-modal').html(data);
           $.unblockUI();
         },
         error: function (jqXHR, textStatus, errorThrown)
         {
           alert('Error adding / update data');
           $.unblockUI();
         }
      });
    }

    function open_modal_2()
    {
         $('#myProfile').modal('show');
    }


  </script>
<!-- endbuild -->
</body>
</html>
