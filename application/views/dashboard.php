<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="materialize is a material design based mutipurpose responsive template">
        <meta name="keywords" content="material design, card style, material template, portfolio, corporate, business, creative, agency">
        <meta name="author" content="trendytheme.net">

        <title>GUDTRAK ecosystem</title>

        <?php include('layout/homepage/head_script.php'); ?>

        <style type="text/css">
          .mb-30 {
          margin-bottom: -13px !important;
          }
          .banner-4 {
              background-image: url("<?php echo base_url()?>assets/main_menu/images/truck2.png");  
          }
        </style>
    </head>

    <body id="top" class="has-header-search">

        <!--header start-->
        <header id="header" class="tt-nav transparent-header">

            <div class="header-sticky light-header">

                <div class="container">
                
                    <div id="materialize-menu" class="menuzord">

                        <!--logo start-->
                        <a class="logo-brand">
                            <img src="<?php echo base_url()?>assets/main_menu/images/GUDAsset 3.png" alt=""/>
                        </a>
                        <!--logo end-->

                        <!--mega menu start-->
                        <ul class="menuzord-menu pull-right">
                            <li><a href="javascript:void(0)" style="color:#03a9f4 !important">
                                <span><i class="fa fa-cog" aria-hidden="true"></i></span> Settings
                              </a>
                                <ul class="dropdown">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user" aria-hidden="true"></i> Profil
                                        </a>
                                    </li>
                                    <li>

                                        <a href="<?=base_url()?>dashboard/logout">
                                            <i class="fa fa-power-off" aria-hidden="true"></i> Keluar
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!--mega menu end-->

                    </div>
                </div>
            </div>
        </header>
        <!--header end-->

        <!-- start revolution slider 5.0 -->

        <!-- Hero Section -->
        <section class="banner-4 parallax-bg bg-fixed overlay dark-5 fullscreen-banner valign-wrapper" data-stellar-background-ratio="0.5">
            <div class="valign-cell">
              <div class="container padding-top-110">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="intro-title text-uppercase white-text mb-30">
                          “Live in Persistant & Consistant”
                        </h1>
                    </div><!-- col-md-6 -->
                </div><!-- row -->
              </div><!-- /.container -->
            </div><!-- /.valign-cell -->
        </section>
        <!-- Hero Section End -->


        <section class="section-padding">
            <div class="container">

              <div class="text-center mb-80">
                  <h2 class="section-title text-uppercase"><i>GUD</i><b>TRAK</b></h2>
                  <p class="section-sub">GUDTRAK, is an ecosystem solution that connects many process business entities to eliminate fraud, and also produces efficient solutions for businesses.</p>
              </div>

              <div class="clients-grid gutter">
                <div class="row">

                  <div class="col-md-3 col-sm-6">
                      <div class="border-box">
                          <a href="<?php echo base_url()?>dashboard/guddata">
                            <img src="<?php echo base_url()?>assets/main_menu/images/icons/10.png" width="70">
                          </a>
                      </div><!-- /.border-box -->
                  </div><!-- /.col-md-3 -->

                  <div class="col-md-3 col-sm-6">
                      <div class="border-box">
                          <a href="<?php echo 'http://10.8.3.219:8087'?>">
                            <img src="<?php echo base_url()?>assets/main_menu/images/icons/6.png" width="70">
                          </a>
                      </div><!-- /.border-box -->
                  </div><!-- /.col-md-3 -->

                  <div class="col-md-3 col-sm-6">
                      <div class="border-box">
                          <a href="#">
                            <img src="<?php echo base_url()?>assets/main_menu/images/icons/5.png" width="70">
                          </a>
                      </div><!-- /.border-box -->
                  </div><!-- /.col-md-3 -->

                  <div class="col-md-3 col-sm-6">
                      <div class="border-box">
                          <a href="#">
                            <img src="<?php echo base_url()?>assets/main_menu/images/icons/7.png" width="70">
                          </a>
                      </div><!-- /.border-box -->
                  </div><!-- /.col-md-3 -->

                  <div class="col-md-3 col-sm-6">
                      <div class="border-box">
                          <a href="#">
                            <img src="<?php echo base_url()?>assets/main_menu/images/icons/11.png" width="70">
                          </a>
                      </div><!-- /.border-box -->
                  </div><!-- /.col-md-3 -->

                  <div class="col-md-3 col-sm-6">
                      <div class="border-box">
                          <a href="#">
                            <img src="<?php echo base_url()?>assets/main_menu/images/icons/9.png" width="70">
                          </a>
                      </div><!-- /.border-box -->
                  </div><!-- /.col-md-3 -->

                  <div class="col-md-3 col-sm-6">
                      <div class="border-box">
                          <a href="#">
                            <img src="<?php echo base_url()?>assets/main_menu/images/icons/4.png" width="70">
                          </a>
                      </div><!-- /.border-box -->
                  </div><!-- /.col-md-3 -->

                  <div class="col-md-3 col-sm-6">
                      <div class="border-box">
                          <a href="#">
                            <img src="<?php echo base_url()?>assets/main_menu/images/icons/8.png" width="70">
                          </a>
                      </div><!-- /.border-box -->
                  </div><!-- /.col-md-3 -->

                </div><!-- /.row -->
              </div><!-- /.clients-grid -->

            </div><!-- /.container -->
        </section>


        <footer class="footer footer-four">
            <div class="primary-footer brand-bg text-center">
                <div class="container">

                  <a href="#top" class="page-scroll btn-floating btn-large pink back-top waves-effect waves-light tt-animate btt" data-section="#top">
                    <i class="material-icons">&#xE316;</i>
                  </a>

                  <ul class="social-link tt-animate ltr mt-20">
                    <li>
                      <center>
                        <a href="#"><i class="fa fa-phone" aria-hidden="true"></i></a>
                        <span style="color: #fff">(021)1500950</span>
                      </center>
                    </li>
                    <li>
                      <center>
                        <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                        <span style="color: #fff">tyms@amoeba.com</span>
                      </center>
                    </li>
                  </ul>

                  <hr class="mt-15">

                  <div class="row">
                    <div class="col-md-12">
                          <div class="footer-logo">
                            <img src="<?php echo base_url()?>assets/images/icon_putih.png" alt=""/>
                          </div>

                          <span class="copy-text">
                            Copyright &copy; 2019 <a>GUD TRAK</a> &nbsp; | &nbsp;  All Rights Reserved
                          </span>
                          <div class="footer-intro">
                            <p>
                              Ready to upgrade your system with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!
                            </p>
                          </div>
                    </div><!-- /.col-md-12 -->
                  </div><!-- /.row -->
                  <br>
                </div><!-- /.container -->
            </div><!-- /.primary-footer -->
        </footer>


        <!-- Preloader -->
        <div id="preloader">
          <div class="preloader-position"> 
            <img src="<?php echo base_url()?>assets/main_menu/images/GUDAsset 3.png" alt="logo" >
            <div class="progress">
              <div class="indeterminate"></div>
            </div>
          </div>
        </div>
        <!-- End Preloader -->

        <?php include('layout/homepage/foot_script.php'); ?>
        
    </body>
  

</html>