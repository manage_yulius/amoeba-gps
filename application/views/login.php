<!DOCTYPE html>
<html lang="en">
<head>
	<title>GUDTRAK ecosystem</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url()?>assets/images/icong.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/main.css">
<!--===============================================================================================-->
</head>
<body style="background-color: #666666;">
<input type="hidden" name="url" value="<?php echo site_url('dashboard')?>" id="url">
<input type="hidden" name="api" value="http://10.8.3.219:8087/api/session" id="api">
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" id="login_form" method="post">
					<span class="login100-form-title p-b-43">
						<img src="<?php echo base_url()?>assets/images/GUDAsset 3.png">
					</span>
					
					
					<div class="wrap-input100 validate-input" >
					<!-- data-validate = "Valid email is required: ex@abc.xyz" -->
						<input class="input100" id="email" type="text" name="email">
						<span class="focus-input100"></span>
						<span class="label-input100">Email</span>
					</div>
					
					
					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<input class="input100" id="password" type="password" name="password">
						<span class="focus-input100"></span>
						<span class="label-input100">Password</span>
					</div>

					<div class="flex-sb-m w-full p-t-3 p-b-32">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div>
					</div>
			

					<!-- <a href="<?php echo base_url()?>assets/main-menu.html" style="width: 100% !important"> -->
						<div class="container-login100-form-btn">
							<button class="login100-form-btn" type="button" onclick="do_login()">
								Login
							</button>
						</div>
					<!-- </a> -->
					
					<div class="text-center p-t-46 p-b-20">
						<span class="txt2">
							or login with
						</span>
					</div>

					<div class="login100-form-social flex-c-m">
						<a href="<?php echo base_url()?>assets/#" class="login100-form-social-item flex-c-m bg1 m-r-5">
							<img src="<?php echo base_url()?>assets/images/google.png" width="70">
						</a>

						<a href="<?php echo base_url()?>assets/#" class="login100-form-social-item flex-c-m bg2 m-r-5">
							<img src="<?php echo base_url()?>assets/images/LOGO SIAB big.png" width="50">
						</a>
					</div>
					
					<div class="text-center p-t-46 p-b-20">
						<span class="txt2">
							Not member? <i><a href="<?php echo base_url()?>assets/#">Sign up here</a></i>
						</span>
					</div>
				</form>

				<div class="login100-more" style="background-image: url('assets/images/truck2.png');">
				</div>
			</div>
		</div>
	</div>
	

	<script type="text/javascript">
		function do_login()
		{
			$.blockUI({ message: '<span style="font-weight:bold;">Permintaan Anda Sedang Diproses...</span>' });
			login();
		}
	</script>

	<script src="<?php echo base_url()?>assets/js/app.js"></script>
	<script src="<?php echo base_url()?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url()?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url()?>assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url()?>assets/js/main.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery.blockui.min.js" type="text/javascript"></script>

</body>
</html>